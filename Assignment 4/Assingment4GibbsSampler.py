"""
Authors:
Rinze Hallema, 5280133
Klaudia Hovrath, 6081487
Nadim Maraqten, 6117856
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

##############################################
### TASK (a) ###
##############################################

def generate_gmm_data(n, K, d, pi, mu, Sigma):
    """
    Generate n independent observations from a Gaussian mixture model.

    Parameters:
    n (int): Number of observations.
    K (int): Number of clusters.
    d (int): Number of dimensions.
    pi (list): Cluster probabilities, length K.
    mu (list): List of mean vectors, each of length d.
    Sigma (ndarray): Covariance matrix of shape (d, d).

    Returns:
    ndarray: Generated data of shape (n, d).
    ndarray: Cluster labels for each data point of shape (n,).
    """
    assert len(pi) == K, "Length of pi should be K"
    assert len(mu) == K, "Length of mu should be K"
    assert Sigma.shape == (d, d), "Sigma should be of shape (d, d)"
    
    # Ensure probabilities sum to 1
    pi = np.array(pi)
    pi /= pi.sum()

    # Generate cluster labels
    labels = np.random.choice(K, size=n, p=pi)

    # Generate data
    data = np.zeros((n, d))
    for i in range(K):
        cluster_data = np.random.multivariate_normal(mu[i], Sigma, size=(labels == i).sum())
        data[labels == i, :] = cluster_data
    
    return data, labels


##############################################
### TASK (b) ###
##############################################


# Parameters
n_train = 1000
n_test = 100
K = 5
d = 2

# Define cluster probabilities (equal probabilities for simplicity)
pi = [1/K] * K

# Define mean vectors for 5 clusters
mu = [
    np.array([0, 0]),
    np.array([5, 5]),
    np.array([-5, -5]),
    np.array([5, -5]),
    np.array([-5, 5])
]

# Define a covariance matrix (identical for all clusters for simplicity)
Sigma = 3*np.array([[1, 0.5], [0.5, 1]])

# Generate training data
train_data, train_labels = generate_gmm_data(n_train, K, d, pi, mu, Sigma)

# Generate test data
test_data, test_labels = generate_gmm_data(n_test, K, d, pi, mu, Sigma)


##############################################
### TASK (c) ###
##############################################

# Combine the data and labels into a DataFrame for easier plotting
train_df = pd.DataFrame(train_data, columns=['X1', 'X2'])
train_df['Cluster'] = train_labels

# Plotting the pair plot (matrix scatter plot)
p = sns.pairplot(train_df, hue='Cluster', palette='viridis')
#plt.suptitle('Matrix Scatter Plot of Training Data')
f = p.fig
f.savefig("GeneratedClusters.png")


##############################################
### TASK (g) ###
##############################################



def sample_mu_k(z, x, k, mu_prior_mean, mu_prior_cov, Sigma):
    """
    Sample mu_k from its conditional posterior distribution.
    """
    n_k = np.sum(z == k)
    if n_k == 0:
        return np.random.multivariate_normal(mu_prior_mean, mu_prior_cov)
    x_k = x[z == k]
    Sigma_inv = np.linalg.inv(Sigma)
    mu_prior_cov_inv = np.linalg.inv(mu_prior_cov)
    
    posterior_cov = np.linalg.inv(mu_prior_cov_inv + n_k * Sigma_inv)
    posterior_mean = posterior_cov @ (Sigma_inv @ x_k.sum(axis=0) + mu_prior_cov_inv @ mu_prior_mean)
    
    return np.random.multivariate_normal(posterior_mean, posterior_cov)

def sample_z(x, mu, Sigma, pi):
    """
    Sample z from its conditional posterior distribution.
    """
    K = len(mu)
    n = x.shape[0]
    z = np.zeros(n, dtype=int)
    Sigma_inv = np.linalg.inv(Sigma)
    for i in range(n):
        log_probs = np.zeros(K)
        for k in range(K):
            diff = x[i] - mu[k]
            log_probs[k] = np.log(pi[k]) - 0.5 * diff.T @ Sigma_inv @ diff
        log_probs -= np.max(log_probs)  # for numerical stability
        probs = np.exp(log_probs)
        probs /= np.sum(probs)
        z[i] = np.random.choice(K, p=probs)
    return z

# Initialize parameters
n_iter = 100
mu_prior_mean = np.zeros(d)
mu_prior_cov = np.eye(d)

# Random initialization
mu_samples = np.zeros((n_iter, K, d))
mu_current = np.random.randn(K, d)
z_current = np.random.choice(K, size=n_train, p=pi)

for t in range(n_iter):
    for k in range(K):
        mu_current[k] = sample_mu_k(z_current, train_data, k, mu_prior_mean, mu_prior_cov, Sigma)
    z_current = sample_z(train_data, mu_current, Sigma, pi)
    mu_samples[t] = mu_current

##############################################
### TASK (h) ###
##############################################

# Visualize the sampled cluster means
plt.rcParams.update({'font.size': 20})

f = plt.figure(figsize=(15, 8))
ax = plt.subplot(111)

for k in range(K):
    ax.scatter(mu_samples[:, k, 0], mu_samples[:, k, 1], label=f'Cluster {k+1}')
plt.xlabel('X1')
plt.ylabel('X2')
#plt.title('Sampled Cluster Means')
ax.legend(loc='center right', bbox_to_anchor=(1.1, 0.5), frameon=False)
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(True)
ax.spines['left'].set_visible(True)
plt.tight_layout()

f.savefig('SampledClusters.png')


##############################################
### TASK (m) ###
##############################################

def variational_inference_gmm(train_data, K, d, Sigma, max_iter=100, tol=1e-6):
    n = train_data.shape[0]
    # Initialize variational parameters
    mu_prior_mean = np.zeros(d)
    mu_prior_cov = np.eye(d)
    
    m = np.random.randn(K, d)
    S = np.array([np.eye(d) for _ in range(K)])
    q_z = np.full((n, K), 1/K)
    pi = np.full(K, 1/K)  # Initialize mixing coefficients
    
    def update_q_theta():
        nonlocal m, S
        for k in range(K):
            Lambda_k_inv = np.linalg.inv(np.linalg.inv(mu_prior_cov) + np.sum(q_z[:, k]) * np.linalg.inv(Sigma))
            m[k] = Lambda_k_inv @ (np.linalg.inv(Sigma) @ (train_data.T @ q_z[:, k]) + np.linalg.inv(mu_prior_cov) @ mu_prior_mean)
            S[k] = Lambda_k_inv
    
    def update_q_z():
        nonlocal q_z, pi
        for i in range(n):
            for k in range(K):
                q_z[i, k] = np.log(pi[k]) - 0.5 * np.trace(np.linalg.inv(Sigma) @ S[k]) - 0.5 * (train_data[i] - m[k]).T @ np.linalg.inv(Sigma) @ (train_data[i] - m[k])
            q_z[i] = np.exp(q_z[i] - np.max(q_z[i]))
            q_z[i] /= np.sum(q_z[i])
        pi = np.mean(q_z, axis=0)  # Update mixing coefficients
    
    elbo_values = []
    for iteration in range(max_iter):
        update_q_theta()
        update_q_z()

        diff = train_data[:, None, :] - m  # Shape (n, K, d)
        term1 = np.einsum('ijk,kl->ijl', diff, np.linalg.inv(Sigma))  # Shape (n, K, d)
        term2 = np.einsum('ijk,ijk->ij', term1, diff)  # Shape (n, K)
        
        # ELBO calculation
        elbo = np.sum(q_z * (np.log(pi) - 0.5 * np.array([np.trace(np.linalg.inv(Sigma) @ S[k]) for k in range(K)])[None, :] - 0.5 * term2))
                
        elbo_values.append(elbo)
        
        if iteration > 0 and np.abs(elbo_values[-1] - elbo_values[-2]) < tol:
            print(f'Converged at iteration {iteration}')
            break
    
    return m, S, q_z, elbo_values


# Run Variational Inference
m, S, q_z, elbo_values = variational_inference_gmm(train_data, K, d, Sigma)

# Visualize the obtained cluster means
plt.figure(figsize=(10, 8))
for k in range(K):
    plt.scatter(m[k, 0], m[k, 1], label=f'Cluster {k+1}', s=100)
plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Variational Inference: Cluster Means')
plt.legend()
plt.show()

# Plot ELBO values
plt.figure(figsize=(10, 8))
plt.plot(elbo_values)
plt.xlabel('Iteration')
plt.ylabel('ELBO')
plt.title('ELBO Convergence')
plt.show()
