"""
Authors:
Rinze Hallema, 5280133
Klaudia Hovrath, 6081487
Nadim Maraqten, 6117856
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.patches import Ellipse


np.random.seed(42)

##############################################
### TASK (a) ###
##############################################

def generate_gmm_data(n, K, d, pi, mu, Sigma_list):
    """
    Generate n independent observations from a Gaussian mixture model.

    Parameters:
    n (int): Number of observations.
    K (int): Number of clusters.
    d (int): Number of dimensions.
    pi (list): Cluster probabilities, length K.
    mu (list): List of mean vectors, each of length d.
    Sigma_list (list): List of covariance matrices, each of shape (d, d).

    Returns:
    ndarray: Generated data of shape (n, d).
    ndarray: Cluster labels for each data point of shape (n,).
    """
    assert len(pi) == K, "Length of pi should be K"
    assert len(mu) == K, "Length of mu should be K"
    assert len(Sigma_list) == K, "Length of Sigma_list should be K"
    
    # Ensure probabilities sum to 1
    pi = np.array(pi)
    pi /= pi.sum()

    # Generate cluster labels
    labels = np.random.choice(K, size=n, p=pi)

    # Generate data
    data = np.zeros((n, d))
    for i in range(K):
        cluster_data = np.random.multivariate_normal(mu[i], Sigma_list[i], size=(labels == i).sum())
        data[labels == i, :] = cluster_data
    
    return data, labels


##############################################
### TASK (b) ###
##############################################


# Parameters
n_train = 100
n_test = 10
K = 5
d = 2

# Define cluster probabilities 
pi = [1/K] *K

# Define mean vectors for 5 clusters
mu = [
    np.array([0, 0]),
    np.array([5, 5]),
    np.array([-5, -5]),
    np.array([5, -5]),
    np.array([-5, 5])
]

# Define individual covariance matrices for each cluster
Sigma_list = [
    3*np.array([[1, 0.5], 
                [0.5, 1]]),
    2*np.array([[1, -0.3], 
                [-0.3, 1]]),
    np.array([[1.5, 0.4], 
              [0.4, 1.5]]),
    np.array([[1, 0], 
              [0, 1]]),
    np.array([[2, 0.2], 
              [0.2, 2]])
]

# Generate training data
train_data, train_labels = generate_gmm_data(n_train, K, d, pi, mu, Sigma_list)

# Generate test data
test_data, test_labels = generate_gmm_data(n_test, K, d, pi, mu, Sigma_list)


##############################################
### TASK (c) ###
##############################################

# Combine the data and labels into a DataFrame for easier plotting
train_df = pd.DataFrame(train_data, columns=['X1', 'X2'])
train_df['Cluster'] = train_labels

# Plotting the pair plot (matrix scatter plot)
p = sns.pairplot(train_df, hue='Cluster', palette='viridis')
#plt.suptitle('Matrix Scatter Plot of Training Data')
f = p.fig
f.savefig("GeneratedClusters.png")


##############################################
### TASK (g) ###
##############################################



def sample_mu_k(z, x, k, mu_prior_mean, mu_prior_cov, Sigma_k):
    """
    Sample mu_k from its conditional posterior distribution.
    """
    n_k = np.sum(z == k)
    if n_k == 0:
        return np.random.multivariate_normal(mu_prior_mean, mu_prior_cov)
    x_k = x[z == k]
    Sigma_inv = np.linalg.inv(Sigma_k)
    mu_prior_cov_inv = np.linalg.inv(mu_prior_cov)
    
    posterior_cov = np.linalg.inv(mu_prior_cov_inv + n_k * Sigma_inv)
    posterior_mean = posterior_cov @ (Sigma_inv @ x_k.sum(axis=0) + mu_prior_cov_inv @ mu_prior_mean)
    
    return np.random.multivariate_normal(posterior_mean, posterior_cov)

def sample_z(x, mu, Sigma_list, pi):
    """
    Sample z from its conditional posterior distribution.
    """
    K = len(mu)
    n = x.shape[0]
    z = np.random.choice(K, size=n)
    for i in range(n):
        log_probs = np.zeros(K)
        for k in range(K):
            Sigma_inv = np.linalg.inv(Sigma_list[k])
            diff = x[i] - mu[k]
            log_probs[k] = np.log(pi[k]) - 0.5 * diff.T @ Sigma_inv @ diff
        log_probs -= np.max(log_probs)  # for numerical stability
        probs = np.exp(log_probs)
        probs /= np.sum(probs)
        z[i] = np.random.choice(K, p=probs)
    return z

# Initialize parameters
n_iter = 100
mu_prior_mean = np.random.randn(d)
mu_prior_cov = np.eye(d) + np.random.randn(d, d) * 0.1

# Random initialization
mu_samples = np.random.randn(n_iter, K, d)
mu_current = np.random.randn(K, d)
z_current = np.random.choice(K, size=n_train, p=pi)

for t in range(n_iter):
    for k in range(K):
        mu_current[k] = sample_mu_k(z_current, train_data, k, mu_prior_mean, mu_prior_cov, Sigma_list[k])
    z_current = sample_z(train_data, mu_current, Sigma_list, pi)
    mu_samples[t] = mu_current

##############################################
### TASK (h) ###
##############################################

# Visualize the sampled cluster means
plt.rcParams.update({'font.size': 20})

f = plt.figure(figsize=(15, 8))
ax = plt.subplot(111)

for k in range(K):
    ax.scatter(mu_samples[:, k, 0], mu_samples[:, k, 1], label=f'Cluster {k+1}')
plt.xlabel('X1')
plt.ylabel('X2')
#plt.title('Sampled Cluster Means')
ax.legend(loc='center right', bbox_to_anchor=(1.1, 0.5), frameon=False)
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['bottom'].set_visible(True)
ax.spines['left'].set_visible(True)
plt.tight_layout()

f.savefig('SampledClusters.png')


##############################################
### TASK (m) ###
##############################################

def variational_inference_gmm(train_data, K, d, Sigma_list, max_iter=100, tol=1e-10):
    n = train_data.shape[0]
    # Initialize variational parameters
    mu_prior_mean = 10*np.random.rand(d)
    mu_prior_cov  = 10*np.eye(d) + 10*np.random.randn(d, d)
    
    m = 10*np.random.randn(K, d)
    S = np.array([np.eye(d) for _ in range(K)])
    q_z = np.full((n, K), 1/K)
    pi = np.full(K, 1/K)  # Initialize mixing coefficients
    
    def update_q_theta():
        nonlocal m, S
        for k in range(K):
            Lambda_k_inv = np.linalg.inv(np.linalg.inv(mu_prior_cov) + np.sum(q_z[:, k]) * np.linalg.inv(Sigma_list[k]))
            m[k] = Lambda_k_inv @ (np.linalg.inv(Sigma_list[k]) @ np.sum(q_z[:, k][:, None] * train_data, axis=0) + np.linalg.inv(mu_prior_cov) @ mu_prior_mean)            
            S[k] = Lambda_k_inv
    
    def update_q_z():
        nonlocal q_z, pi
        for i in range(n):
            for k in range(K):
                q_z[i, k] = np.log(pi[k]) - 0.5 * np.trace(np.linalg.inv(Sigma_list[k]) @ S[k]) - 0.5 * (train_data[i] - m[k]).T @ np.linalg.inv(Sigma_list[k]) @ (train_data[i] - m[k])
            q_z[i] = np.exp(q_z[i] - np.max(q_z[i]))
            q_z[i] /= np.sum(q_z[i])
        pi = np.mean(q_z, axis=0)  # Update mixing coefficients
    
    elbo_values = []
    for iteration in range(max_iter):
        update_q_theta()
        update_q_z()
     
        # Precompute inverses of covariance matrices
        Sigma_inverses = np.array([np.linalg.inv(Sigma_list[k]) for k in range(K)])

        diff = train_data[:, None, :] - m  # Shape (n, K, d)
        term1 = np.einsum('ijk,jkl->ijl', diff, Sigma_inverses)  # Shape (n, K, d)
        term2 = np.einsum('ijk,ijk->ij', term1, diff)  # Shape (n, K)

        
        # ELBO calculation
        elbo = np.sum(q_z * (np.log(pi) - 0.5 * np.array([np.trace(Sigma_inverses[k] @ S[k]) for k in range(K)])[None, :] - 0.5 * term2))
                
        elbo_values.append(elbo)
        
        # Add a minimum number of iterations before checking for convergence
        min_iterations = 50
        if iteration > min_iterations and np.abs(elbo_values[-1] - elbo_values[-2]) < tol:
            print(f'Converged at iteration {iteration}')
            break

    return m, S, q_z, elbo_values, mu_prior_mean, mu_prior_cov

# Run Variational Inference
m, S, q_z, elbo_values, mu_prior_mean, mu_prior_cov = variational_inference_gmm(train_data, K, d, Sigma_list)
print("m size:", m.shape)
print("m values:")
print(m)
print("S size:", S.shape)
print("S values:")
print(S)
print("q_z size:", q_z.shape)
print("elbo_values size:", len(elbo_values))

##############################################
### TASK (n) ###
##############################################

def plot_gaussian_ellipse(mean, cov, ax, n_std=2.0, facecolor='none', **kwargs):
    """Plot an ellipse for a Gaussian distribution defined by its mean and covariance."""
    pearson = cov[0, 1] / np.sqrt(cov[0, 0] * cov[1, 1])
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0), width=ell_radius_x * 2, height=ell_radius_y * 2,
                      facecolor=facecolor, **kwargs)
    
    scale_x = np.sqrt(cov[0, 0]) * n_std
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_x, mean_y = mean

    transf = (plt.matplotlib.transforms.Affine2D()
              .rotate_deg(45)
              .scale(scale_x, scale_y)
              .translate(mean_x, mean_y))

    ellipse.set_transform(transf + ax.transData)
    ax.add_patch(ellipse)
# Visualize the obtained cluster means and posterior variances
plt.figure(figsize=(10, 8))
ax = plt.gca()
for k in range(K):
    plt.scatter(m[k, 0], m[k, 1], label=f'Cluster {k+1}', s=100)
    plot_gaussian_ellipse(m[k], S[k], ax, edgecolor='red')

plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Variational Inference: Cluster Means and Posterior Variances')
plt.legend(loc='lower right', bbox_to_anchor=(1.1, 0.8))
#plt.show()
plt.savefig('VariationalInference.png')

# Plot ELBO values
plt.figure(figsize=(10, 8))
plt.plot(elbo_values)
plt.xlabel('Iteration')
plt.ylabel('ELBO')
plt.title('ELBO Convergence')
#plt.show()
plt.savefig('ELBOConvergence.png')
##############################################
### TASK (o) ###
##############################################

# Function to calculate the log likelihood for the test set using Gibbs samples
def compute_log_likelihood_gibbs(test_data, mu_samples, Sigma_list):
    n_test, d = test_data.shape
    n_samples, K, _ = mu_samples.shape

    log_likelihood = 0
    for i in range(n_test):
        sample_likelihoods = []
        for s in range(n_samples):
            likelihood = 0
            for k in range(K):
                Sigma_inv = np.linalg.inv(Sigma_list[k])
                diff = test_data[i] - mu_samples[s, k]
                likelihood += np.exp(-0.5 * diff.T @ Sigma_inv @ diff)
            sample_likelihoods.append(np.log(likelihood / K))
        log_likelihood += np.log(np.sum(np.exp(sample_likelihoods)) / n_samples)
    
    return log_likelihood

# Calculate log L_Gibbs
log_L_gibbs = compute_log_likelihood_gibbs(test_data, mu_samples, Sigma_list)
print(f"log L_Gibbs: {log_L_gibbs}")


##############################################
### TASK (p) ###
##############################################

def compute_log_likelihood_variational(train_data, K, d, m, S, Sigma_list, pi, mu_prior_mean, mu_prior_cov):
    n = train_data.shape[0]
    Sigma_inverses = np.array([np.linalg.inv(Sigma_list[k]) for k in range(K)])
    
    # Compute the expected log likelihood term
    diff = train_data[:, None, :] - m  # Shape (n, K, d)
    term1 = np.einsum('ijk,jkl->ijl', diff, Sigma_inverses)  # Shape (n, K, d)
    term2 = np.einsum('ijk,ijk->ij', term1, diff)  # Shape (n, K)
    
    expected_log_likelihood = np.sum(q_z * (np.log(pi) - 0.5 * np.array([np.trace(Sigma_inverses[k] @ S[k]) for k in range(K)])[None, :] - 0.5 * term2))
    
    # Compute the KL divergence term
    kl_divergence = 0
    for k in range(K):
        kl_divergence += 0.5 * (np.trace(np.linalg.inv(mu_prior_cov) @ S[k]) + 
                                (m[k] - mu_prior_mean).T @ np.linalg.inv(mu_prior_cov) @ (m[k] - mu_prior_mean) - d + 
                                np.log(np.linalg.det(mu_prior_cov) / np.linalg.det(S[k])))
    
    elbo = expected_log_likelihood - kl_divergence
    return elbo

# Calculate the log L_Variational
log_L_variational = compute_log_likelihood_variational(train_data, K, d, m, S, Sigma_list, pi, mu_prior_mean, mu_prior_cov)
print(f"log L_Variational: {log_L_variational}")
