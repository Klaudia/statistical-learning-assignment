#@author: Your names and student numbers
"""
Rinze Hallema - 5280133
Klaudia Horvath - 6081487
Nadim Maraqten - 6117856
""" 

#import packages

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from pathlib import Path
from sklearn.model_selection import StratifiedKFold
import os

plt.rcParams.update({'font.size': plt.rcParams['font.size']*1.5})
path = Path(__file__).parent / "data/A3_spam/spam.txt"

#read the txt file 
df = pd.read_csv(path)
print(df)

#make dataframe into numpy array
df.to_numpy()
target = 'spam'
columns = ['word_freq_make', 'word_freq_address', 'word_freq_all',
           'word_freq_3d', 'word_freq_our', 'word_freq_over',
           'word_freq_remove', 'word_freq_internet', 'word_freq_order',
           'word_freq_mail', 'word_freq_receive', 'word_freq_will',
           'word_freq_people', 'word_freq_report', 'word_freq_addresses',
           'word_freq_free', 'word_freq_business', 'word_freq_email',
           'word_freq_you', 'word_freq_credit', 'word_freq_your',
           'word_freq_font', 'word_freq_000', 'word_freq_money',
           'word_freq_hp', 'word_freq_hpl', 'word_freq_george',
           'word_freq_650', 'word_freq_lab', 'word_freq_labs',
           'word_freq_telnet', 'word_freq_857', 'word_freq_data',
           'word_freq_415', 'word_freq_85', 'word_freq_technology',
           'word_freq_1999', 'word_freq_parts', 'word_freq_pm',
           'word_freq_direct', 'word_freq_cs', 'word_freq_meeting',
           'word_freq_original', 'word_freq_project', 'word_freq_re',
           'word_freq_edu', 'word_freq_table', 'word_freq_conference',
           'char_freq_;', 'char_freq_(', 'char_freq_[', 'char_freq_!',
           'char_freq_$', 'char_freq_#', 'capital_run_length_average',
           'capital_run_length_longest', 'capital_run_length_total']


x= df[columns].values
y= df[target].values



test_set = df['test'].to_numpy()
x_train, x_test = x[test_set == 0], x[test_set == 1]
y_train, y_test = y[test_set == 0], y[test_set == 1]

test_set

#### Which tasks shall be executed? ####
perform_1a, perform_1b, perform_1c, perform_1e, perform_1f = True, False, False, True, False


### TASK 1 (a) ###

if perform_1a == True:
    # Logistic Regression
    # Fit a logistic regression model to the spam dataset using the Python function LogisticRegression
    # and compute the prediction accuracy over the test set using the function accuracy score.
    #clf = LogisticRegression().fit(x_train, y_train)
    logreg = LogisticRegression(solver='newton-cg')

    # fit the model with data
    log_reg_model = logreg.fit(x_train, y_train)
    y_pred = logreg.predict(x_test)
    y_true = y_test
    acc_score = accuracy_score(y_true, y_pred)
    print("\n-----------------\nAccuracy score of logistic regression {}".format(acc_score))

### TASK 1 (b) ###

if perform_1b == True:

    # Decision tree fitting
    clf = DecisionTreeClassifier()
    clf.fit(x_train, y_train)
    y_pred_tree = clf.predict(x_test)
    acc_score = accuracy_score(y_test, y_pred_tree)
    print(f"\n-----------------\nAccuracy score of decision tree: {acc_score}")

    # Function for cross-validation for varying number of terminal nodes

    def perform_crossvalidation_for_range_of_tuning_parameter(x_train, y_train, K):
        # Define the range for the tuning parameter, which is max_leaf_nodes in this case
        tuning_parameter_range = np.arange(2, 100)
        
        # Variables to store results
        cross_validation_estimated_accuracy = []
        cross_validation_estimated_accuracy_standard_deviation = []

        # Setting up the StratifiedKFold
        skf = StratifiedKFold(n_splits=K)

        # Cross-validation for varying number of terminal nodes
        for max_leaf_nodes in tuning_parameter_range:
            print(f"max_leaf_nodes = {max_leaf_nodes}")
            accuracy_scores = []

            # Splitting data into K folds
            for train_index, test_index in skf.split(x_train, y_train):
                X_train_fold, X_test_fold = x_train[train_index], x_train[test_index]
                y_train_fold, y_test_fold = y_train[train_index], y_train[test_index]

                # Training the Decision Tree Classifier on the folded data
                clf = DecisionTreeClassifier(max_leaf_nodes=max_leaf_nodes)
                clf.fit(X_train_fold, y_train_fold)
                scores = accuracy_score(y_test_fold, clf.predict(X_test_fold))
                accuracy_scores.append(scores)

            # Calculating mean and standard deviation of accuracy scores
            cross_validation_estimated_accuracy.append(np.mean(accuracy_scores))
            cross_validation_estimated_accuracy_standard_deviation.append(np.std(accuracy_scores))

            # Convert accuracy to generalization error
            cross_validation_estimated_error = [(1 - acc) * 100 for acc in cross_validation_estimated_accuracy]
            cross_validation_estimated_error_standard_deviation = [std * 100 for std in cross_validation_estimated_accuracy_standard_deviation]




        # Plotting the results
        fig = plt.figure(figsize=(10, 6))  # width, height in inches
        ax = fig.add_subplot(1, 1, 1)
        ax.errorbar(tuning_parameter_range, cross_validation_estimated_error, yerr=cross_validation_estimated_error_standard_deviation, capsize=4, fmt='ro--', ecolor='black', elinewidth=0.5)
        ax.set_xlabel('Number of terminal nodes')
        ax.set_ylabel('Generalization Error [%]')
        ax.set_title('CV Estimate of Generalization Error')
        plt.ylim(0, 40)
        plt.grid()
        plt.tight_layout()
        plt.savefig(Path(__file__).parent / "DecisionTree_CV_Generalisation_Error_vs_Nodes.pdf")
        plt.show()

        # Finding the optimal number of terminal nodes
        optimal_index = np.argmax(cross_validation_estimated_accuracy)
        optimal_nodes = tuning_parameter_range[optimal_index]
        optimal_accuracy = cross_validation_estimated_accuracy[optimal_index]
        optimal_std_error = cross_validation_estimated_accuracy_standard_deviation[optimal_index]

        print("\n".join([f"Tuning Parameter: {tp}, Accuracy: {acc:.4f}, Error: {err:.2f}%, Std Deviation: {std:.2f}%" for tp, acc, err, std in zip(tuning_parameter_range, cross_validation_estimated_accuracy, cross_validation_estimated_error, cross_validation_estimated_error_standard_deviation)]))
        
        return print('The optimal number of terminal nodes is:', optimal_nodes, 'with accuracy (error):', optimal_accuracy, "(", (1-optimal_accuracy), ") and standard error:", optimal_std_error)

    perform_crossvalidation_for_range_of_tuning_parameter(x_train, y_train, 5)

    
    
### TASK 1 (c) ###

if perform_1c == True:
    # Fit a random forest to the spam dataset
    rf_clf = RandomForestClassifier(oob_score=True)  # Enable OOB error tracking
    rf_clf.fit(x_train, y_train)
    y_pred_rf = rf_clf.predict(x_test)

    # Compute the prediction accuracy over the test set
    acc_score_rf = accuracy_score(y_test, y_pred_rf)
    print("\n-----------------\nAccuracy score of random forest: {}".format(acc_score_rf))

    # Compute the OOB error
    oob_error = 1 - rf_clf.oob_score_
    print("OOB error of random forest: {}".format(oob_error))


    def cross_validation_rf(x_train, y_train, max_leaf_nodes_range, K=5):
        skf = StratifiedKFold(n_splits=K)
        mean_accuracies = []
        std_errors = []
        oob_errors = []

        for max_leaf_nodes in max_leaf_nodes_range:
            print(f"max_leaf_nodes = {max_leaf_nodes}")
            accuracies = []
            for train_idx, test_idx in skf.split(x_train, y_train):
                X_train_k, X_test_k = x_train[train_idx], x_train[test_idx]
                y_train_k, y_test_k = y_train[train_idx], y_train[test_idx]
                
                model = RandomForestClassifier(max_leaf_nodes=max_leaf_nodes, oob_score=True)
                model.fit(X_train_k, y_train_k)
                accuracies.append(accuracy_score(y_test_k, model.predict(X_test_k)))
            
            mean_accuracies.append(np.mean(accuracies))
            std_errors.append(np.std(accuracies))
            # Collecting OOB error for the current number of terminal nodes
            oob_errors.append(1 - model.oob_score_)

        return max_leaf_nodes_range, mean_accuracies, std_errors, oob_errors

    # Determine the optimal number of terminal nodes
    nodes_range = np.arange(2, 100)#np.arange(2, 50)  # np.linspace(2, 100, 3, dtype=int)

    # Obtain CV results and OOB errors
    nodes, mean_accs, std_devs, oob_errors = cross_validation_rf(x_train, y_train, nodes_range)
    mean_errors = [(1 - acc) * 100 for acc in mean_accs]
    std_devs = [100 * std_dev for std_dev in std_devs]

    # Plotting the estimated generalization error against the number of terminal nodes
    fig = plt.figure(figsize=(10, 6))  # width, height in inches
    plt.errorbar(nodes, mean_errors, yerr=std_devs, fmt='-o', capsize=5, label='Errors with STD')
    plt.xlabel('Allowed Number of Terminal Nodes')
    plt.ylabel('Generalization Error [%]')
    plt.title('Effect of Terminal Nodes on RandomForest Performance')
    plt.ylim(0, 40)
    plt.grid()
    plt.tight_layout()
    plt.savefig(Path(__file__).parent / "RandomForest_CV_Generalisation_Error_vs_Nodes.pdf")
    plt.show()


    print("\n Random Forest Results: \n".join([
        f"Max Leaf Nodes: {node}, Accuracy: {acc:.4f}, Error: {err:.2f}%, Std Deviation: {std:.2f}%, OOB Error: {oob:.2f}%"
        for node, acc, err, std, oob in zip(nodes, mean_accs, mean_errors, std_devs, oob_errors)
    ]))

    # Identify and print the optimal number of terminal nodes
    optimal_nodes = nodes[np.argmax(mean_accs)]
    optimal_accuracy = mean_accs[np.argmax(mean_accs)]
    optimal_std_dev = std_devs[np.argmax(mean_accs)]
    optimal_oob_error = oob_errors[np.argmax(mean_accs)]

    print(f"The optimal number of terminal nodes is {optimal_nodes}, with an accuracy of {optimal_accuracy:.4f} and a standard deviation of {optimal_std_dev:.4f}.")
    print(f"The OOB error of the model with the optimal number of terminal nodes is {optimal_oob_error*100:.4f}.")

### TASK 1 (d) ###

# in report


### TASK 1 (e) ###

if perform_1e == True:
    # Prepare range of m values to test - commonly used range from 1 to sqrt(total number of features)
    m_values = range(1, 51)

    # Lists to store OOB and test errors
    oob_errors = []
    test_errors = []

    for m in m_values:
        # Create RandomForest model with specified max_features
        rf_model = RandomForestClassifier(max_features=m, oob_score=True, random_state=42)
        rf_model.fit(x_train, y_train)

        # Record OOB error
        oob_error = 100 - rf_model.oob_score_*100
        oob_errors.append(oob_error)

        # Predict on test set and calculate test error
        y_pred_test = rf_model.predict(x_test)
        test_error = 100 - accuracy_score(y_test, y_pred_test)*100
        test_errors.append(test_error)

    # Plotting the OOB and test errors against values of m
    plt.figure(figsize=(10, 5))
    plt.plot(m_values, oob_errors, label='OOB Error', marker='o', linestyle='-', color='blue')
    plt.plot(m_values, test_errors, label='Test Error', marker='x', linestyle='--', color='red')
    plt.xlabel('Max Features (m)')
    plt.ylabel('Error [%]')
    plt.title('OOB and Test Error Sensitivity to Max Features per Split in RandomForest')
    plt.legend()
    plt.ylim(0, 10)
    plt.grid(True)
    plt.savefig(Path(__file__).parent / "OOB_and_Test_Errors_vs_max_features.pdf")
    plt.show()

### TASK 1 (f) ###

if perform_1f == True:
    # Function to perform cross-validation
    def cross_validate_mlp(x_train, y_train, hidden_layer_sizes, K=5):
        skf = StratifiedKFold(n_splits=K)
        mean_accuracies = []
        std_errors = []

        for size in hidden_layer_sizes:
            print(f"size = {size}")
            accuracies = []
            for train_index, test_index in skf.split(x_train, y_train):
                X_train_fold, X_test_fold = x_train[train_index], x_train[test_index]
                y_train_fold, y_test_fold = y_train[train_index], y_train[test_index]
                mlp = MLPClassifier(hidden_layer_sizes=(size,), max_iter=1000, random_state=42, activation="logistic")
                mlp.fit(X_train_fold, y_train_fold)
                accuracies.append(100*accuracy_score(y_test_fold, mlp.predict(X_test_fold)))
            mean_accuracies.append(np.mean(accuracies))
            std_errors.append(np.std(accuracies))

        return hidden_layer_sizes, mean_accuracies, std_errors

    # Define a range of hidden layers to evaluate
    hidden_layers = range(10, 101, 10)#range(1,10,1)#range(10, 201, 10)  # From 10 to 200 in steps of 10

    # Perform cross-validation
    hidden_layers, mean_accs, std_devs = cross_validate_mlp(x_train, y_train, hidden_layers)

    # Plotting the results
    plt.errorbar(hidden_layers, mean_accs, yerr=std_devs, fmt='-o', capsize=5)
    plt.xlabel('Number of Hidden Units')
    plt.ylabel('Cross-Validated Accuracy [%]')
    plt.title('Performance of MLP with Varying Hidden Units')
    plt.grid(True)
    plt.ylim(80,100)
    plt.tight_layout()
    plt.savefig(Path(__file__).parent / "MLP_accuracy_vs_varying_hidden_units.pdf")
    plt.show()

    # Find the optimal number of hidden units
    optimal_hidden_units = hidden_layers[np.argmax(mean_accs)]
    optimal_accuracy = mean_accs[np.argmax(mean_accs)]
    optimal_std_dev = std_devs[np.argmax(mean_accs)]

    print(f"The optimal number of hidden units is {optimal_hidden_units} with an accuracy of {optimal_accuracy:.2f} ± {optimal_std_dev:.2f}.")

