
import numpy as np
import matplotlib.pyplot as plt

n = 20
x = np.linspace(0, np.pi, n)
# True function
y = np.sin(x)

xn1 = np.pi / 2
mu = 0
sigma = 0.1

noise = np.random.normal(0, 0.1,
                         n)
noisy_measurement = y + noise

# Kernel function approximation
# Exponential kernel
def eval_kernel_function(x1, x2, theta=1, type="exponential", variance=0.1):
    if type == "exponential":
        k = np.exp(-theta * np.abs(x1 - x2))
    else:  # gaussian
        k = np.exp(-theta * (np.abs(x1 - x2)) ** 2 / (2 * variance))
    return k


def graham_kernel(x, theta, type, variance):
    n = len(x)  # vector of length n
    k = np.zeros((n, n))
    for idx_row, x1 in enumerate(x):
        for idx_col, x2 in enumerate(x):
            k[idx_row, idx_col] = eval_kernel_function(x1, x2, theta=theta,
                                                       type=type, variance=variance)
    return k


def calculate_k(x, xn1, theta, type, variance):
    # This is an n by 1 column vector
    n = len(x)
    k = np.zeros((n, 1))
    for idx_col, x in enumerate(x):
        k[idx_col, 0] = eval_kernel_function(x, xn1, theta=theta, type=type,
                                             variance=variance)
    return k


def calculate_c(xn1, sigma, theta, type, variance=0.1):
    # this is a scalar
    xn1 = xn1
    xn2 = xn1
    c = eval_kernel_function(xn1, xn2, theta=theta, type=type,
                             variance=variance) + sigma ** 2
    return c


def calculate_cn(x, sigma, theta, type, variance=0.1):
    # this is a scalar
    n = len(x)
    cn = graham_kernel(x, theta, type, variance=0.1) + sigma ** 2 * np.eye(n)
    return cn


kernel_type = 'exponential'
g_var = 0.1
theta = 10.0

k = graham_kernel(x, theta=1, type=kernel_type, variance=0.1)
x_eval_len = 100
x_eval = np.linspace(0, np.pi, x_eval_len)
mean_vector_k1 = np.zeros((x_eval_len, 1))
stdv_vector_k1 = np.zeros((x_eval_len, 1))

for idx, x_element in enumerate(x_eval):
    xn1 = x_element
    c = calculate_c(xn1, sigma, theta, kernel_type, g_var)
    k_calc = calculate_k(x, xn1, theta, kernel_type, g_var)
    k_transp = np.transpose(k_calc)
    cn = calculate_cn(x, sigma, theta, kernel_type, g_var)
    cn_inv = np.linalg.inv(cn)
    posterior_mean = np.matmul(np.matmul(k_transp, cn_inv), noisy_measurement)
    posterior_var2 = c - np.matmul(np.matmul(k_transp, cn_inv), k_calc)
    stdv = posterior_var2 ** 0.5
    mean_vector_k1[idx, 0] = posterior_mean
    stdv_vector_k1[idx, 0] = stdv

f2, ax = plt.subplots()
ax.plot(x, noisy_measurement, 'o', color='green', markersize='5',
        label='observations')
ax.plot(x, y, 'r', label='true function')

ax.plot(x_eval, mean_vector_k1, 'b-', label='exponential kernel')
ax.fill_between(x_eval, (mean_vector_k1 - stdv_vector_k1).flatten(),
                (mean_vector_k1 + stdv_vector_k1).flatten(), color='b', alpha=.1)
ax.set_xlim(0, np.pi)
ax.set_xlabel('x')
ax.set_ylabel('sin(x) evaluations')
ax.set_title('Approximation of the sin(x) with exponential kernel functions')
ax.legend()
f2.savefig('function_approximation_exponential_kernel.png')

kernel_type = 'gaussian'
g_var = 0.1
theta = 1.0

k = graham_kernel(x, theta=1, type=kernel_type, variance=0.1)
x_eval_len = 100
x_eval = np.linspace(0, np.pi, x_eval_len)
mean_vector_k1 = np.zeros((x_eval_len, 1))
stdv_vector_k1 = np.zeros((x_eval_len, 1))

for idx, x_element in enumerate(x_eval):
    xn1 = x_element
    c = calculate_c(xn1, sigma, theta, kernel_type, g_var)
    k_calc = calculate_k(x, xn1, theta, kernel_type, g_var)
    k_transp = np.transpose(k_calc)
    cn = calculate_cn(x, sigma, theta, kernel_type, g_var)
    cn_inv = np.linalg.inv(cn)
    posterior_mean = np.matmul(np.matmul(k_transp, cn_inv), noisy_measurement)
    posterior_var2 = c - np.matmul(np.matmul(k_transp, cn_inv), k_calc)
    stdv = posterior_var2 ** 0.5
    mean_vector_k1[idx, 0] = posterior_mean
    stdv_vector_k1[idx, 0] = stdv

f3, ax = plt.subplots()
ax.plot(x, noisy_measurement, 'o', color='green', markersize='5',
        label='observations')
ax.plot(x, y, 'r', label='true function')

ax.plot(x_eval, mean_vector_k1, 'b-', label='gaussian kernel')
ax.fill_between(x_eval, (mean_vector_k1 - stdv_vector_k1).flatten(),
                (mean_vector_k1 + stdv_vector_k1).flatten(), color='b', alpha=.1)
ax.set_xlim(0, np.pi)
ax.set_xlabel('x')
ax.set_ylabel('sin(x) evaluations')
ax.set_title('Approximation of the sin(x) with gaussian kernel functions')
ax.legend()
f3.savefig('function_approximation_gaussian_kernel.png')
