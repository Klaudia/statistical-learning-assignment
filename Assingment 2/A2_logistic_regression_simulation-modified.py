#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 2024

@author: Your names and student numbers
Rinze Hallema - 5280133
Klaudia Horvath - 6081487
Nadim Maraqten - 6117856
"""

# import packages

import numpy as np
import matplotlib.pyplot as plt
from functions_Q1 import logistic_simulation, calculate_MLE_using_IRLS

test_run = False

# n denotes the sample size
n = 1000

# we simulate two types of features
half_sample = int(n / 2)
x1 = np.random.multivariate_normal([-0.5, 1], [[1, 0.7], [0.7, 1]], half_sample)
x2 = np.random.multivariate_normal([2, -1], [[1, 0.7], [0.7, 1]], half_sample)
simulated_features = np.vstack((x1, x2)).astype(np.float64)

# the underlying value of beta in the simulation; the value we want to retrieve in
# the estimation procedure
beta_star = np.array([0.2, -0.8])
simulated_labels = logistic_simulation(simulated_features, beta_star, n)

X = simulated_features
Y = simulated_labels
W_initial = np.eye(1000)

# Exercise 1 a

beta_initial = np.transpose(np.array([0.0, 0.0]))
beta_ex1 = calculate_MLE_using_IRLS(beta_initial, W_initial, X, Y,
                                    num_of_iterations=5, lambda_par=0)
print("Maximum likelihood estimator, beta:{}".format(beta_ex1))

# Exercise 2b
# Monte Carlo experiment using 100 simulations

if test_run:
    num_of_experiments = 5
else:
    num_of_experiments = 100

beta_mc = np.zeros((num_of_experiments, 2))
beta_initial = np.transpose(np.array([0.0, 0.0]))
W_initial = np.eye(n)

for i in range(num_of_experiments):
    print(f"Number of experiment: {i} of {num_of_experiments}")
    Y = logistic_simulation(simulated_features, beta_star, n)
    beta_mc[i, :] = calculate_MLE_using_IRLS(beta_initial, W_initial, X, Y,
                                             num_of_iterations=3, lambda_par=0)

beta_1 = beta_mc[:, 0]
beta_2 = beta_mc[:, 1]
print("The mean of the maximum likelihood estimator after 100 iterations is:{} and {}".format(np.mean(beta_1), np.mean(beta_2)))

beta_mc_100 = np.column_stack((beta_1, beta_2))


# compute the means of estimated parameters beta_1 and beta_2
# make a histogram for the MLE of beta_1 and beta_2
font = {'size': 18}
plt.rc('font', **font)

weights = np.ones_like(beta_1)/float(len(beta_1))
f2 = plt.figure(figsize=(12, 8))
plt.hist(beta_1, bins=10, weights=weights)
plt.xlabel('Beta 1 parameter')
plt.ylabel('Normalized number of outcomes')
plt.title('Approximation of Beta 1 parameter from 100 simulations')
f2.savefig('Beta_1_histogram_100.png')

f3 = plt.figure(figsize=(12, 8))
weights = np.ones_like(beta_2)/float(len(beta_2))
plt.hist(beta_2, bins=10, weights=weights)
plt.xlabel('Beta 2 parameter')
plt.ylabel('Normalized number of outcomes')
plt.title('Approximation of Beta 2 parameter from 100 simulations')
f3.savefig('Beta_2_histogram_100.png')

# Monte Carlo experiment using 100 simulations

if test_run:
    num_of_experiments = 10
else:
    num_of_experiments = 1000
beta_mc = np.zeros((num_of_experiments, 2))
beta_initial = np.transpose(np.array([0.0, 0.0]))
W_initial = np.eye(n)

for i in range(num_of_experiments):
    print(f"Number of experiment: {i} of {num_of_experiments}")
    Y = logistic_simulation(simulated_features, beta_star, n)
    beta_mc[i, :] = calculate_MLE_using_IRLS(beta_initial, W_initial, X, Y,
                                             num_of_iterations=3, lambda_par=0)

beta_1 = beta_mc[:, 0]
beta_2 = beta_mc[:, 1]

beta_mc_1000 = np.column_stack((beta_1, beta_2))

print("The mean of the maximum likelihood estimator after 1000 iterations is:{} and {}".format(np.mean(beta_1), np.mean(beta_2)))

# compute the means of estimated parameters beta_1 and beta_2
# make a histogram for the MLE of beta_1 and beta_2
f4 = plt.figure(figsize=(12, 8))
weights = np.ones_like(beta_1)/float(len(beta_1))
plt.hist(beta_1, bins=10, weights=weights)
plt.xlabel('Beta 1 parameter')
plt.ylabel('Normalized number of outcomes')
plt.title('Approximation of Beta 1 parameter from 1000 simulations')
f4.savefig('Beta_1_histogram_1000.png')

f5 = plt.figure(figsize=(12, 8))
weights = np.ones_like(beta_2)/float(len(beta_2))
plt.hist(beta_2, bins=10, weights=weights)
plt.xlabel('Beta 2 parameter')
plt.ylabel('Normalized number of outcomes')
plt.title('Approximation of Beta 2 parameter from 1000 simulations')
f5.savefig('Beta_2_histogram_1000.png')


########
# Histograms for Beta 1
f1, ax1 = plt.subplots(figsize=(12, 8))
weights_100 = np.ones_like(beta_mc_100[:, 0])/float(len(beta_mc_100[:, 0]))
weights_1000 = np.ones_like(beta_mc_1000[:, 0])/float(len(beta_mc_1000[:, 0]))
ax1.hist(beta_mc_100[:, 0], bins=10, weights=weights_100, alpha=0.5, label='n=100')
ax1.hist(beta_mc_1000[:, 0], bins=10, weights=weights_1000, alpha=0.5, label='n=1000')
ax1.set_xlabel('Beta 1 parameter')
ax1.set_ylabel('Normalized number of outcomes')
ax1.set_title('Approximation of Beta 1 parameter from simulations')
ax1.legend()
f1.savefig('Combined_Beta_1_histogram.png')

# Histograms for Beta 2
f2, ax2 = plt.subplots(figsize=(12, 8))
weights_100 = np.ones_like(beta_mc_100[:, 1])/float(len(beta_mc_100[:, 1]))
weights_1000 = np.ones_like(beta_mc_1000[:, 1])/float(len(beta_mc_1000[:, 1]))
ax2.hist(beta_mc_100[:, 1], bins=10, weights=weights_100, alpha=0.5, label='n=100')
ax2.hist(beta_mc_1000[:, 1], bins=10, weights=weights_1000, alpha=0.5, label='n=1000')
ax2.set_xlabel('Beta 2 parameter')
ax2.set_ylabel('Normalized number of outcomes')
ax2.set_title('Approximation of Beta 2 parameter from simulations')
ax2.legend()
f2.savefig('Combined_Beta_2_histogram.png')