# functions_Q2.py

import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_val_score
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.metrics import log_loss, make_scorer, accuracy_score

def unpickle(file):
    print(f"Starting to unpickle the file: {file}")
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='latin1')
    print(f"Completed unpickling the file: {file}")
    return dict

def load_data(data_dir):
    print(f"Starting to load data from directory: {data_dir}")
    batches = ['data_batch_{}'.format(i) for i in range(1, 6)]
    batch_data = [unpickle(os.path.join(data_dir, batch)) for batch in batches]
    test_batch = unpickle(os.path.join(data_dir, 'test_batch'))
    meta = unpickle(os.path.join(data_dir, 'batches.meta'))

    X_train = np.concatenate([batch['data'] for batch in batch_data])
    y_train = np.concatenate([batch['labels'] for batch in batch_data])
    X_test = test_batch['data']
    y_test = test_batch['labels']
    label_names = meta['label_names']

    print(f"Completed loading data from directory: {data_dir}")
    return X_train, y_train, X_test, y_test, label_names

def plot_image_from_data(X, y, index, label_names):
    print(f"Starting to plot image at index: {index}")
    image = X[index].reshape(3, 32, 32).transpose(1, 2, 0)
    label = label_names[y[index]]
    plt.imshow(image)
    plt.title(label)
    plt.axis('off')
    plt.show()
    print(f"Completed plotting image at index: {index}")

def reduce_dataset(X, y, reduction_factor):
    print(f"Starting to reduce dataset with reduction factor: {reduction_factor}")
    X_reduced, _, y_reduced, _ = train_test_split(X, y, test_size=reduction_factor, random_state=42, stratify=y)
    print(f"Completed reducing dataset with reduction factor: {reduction_factor}")
    return X_reduced, y_reduced

def scale_data(X_train, X_test):
    print("Starting to scale data")
    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(X_train.astype(np.float64))
    X_test_scaled = scaler.transform(X_test.astype(np.float64))
    print("Completed scaling data")
    return X_train_scaled, X_test_scaled

def apply_pca(X_train_scaled, X_test_scaled, n_components):
    print(f"Starting PCA with n_components: {n_components}")
    pca = PCA(n_components=n_components)
    X_train_pca = pca.fit_transform(X_train_scaled)
    X_test_pca = pca.transform(X_test_scaled)
    print(f"Original number of features: {X_train_scaled.shape[1]}")
    print(f"Reduced number of features: {X_train_pca.shape[1]}")
    print(f"Total explained variance by the {n_components} components: {np.sum(pca.explained_variance_ratio_):.2f}")
    print(f"Completed PCA with n_components: {n_components}")
    return X_train_pca, X_test_pca, pca

def perform_logistic_regression(X_train, y_train, C, max_iter=1000):
    print(f"Starting logistic regression with C: {C} and max_iter: {max_iter}")
    model = LogisticRegression(C=C, solver='newton-cg', multi_class='multinomial', max_iter=max_iter)
    model.fit(X_train, y_train)
    print(f"Completed logistic regression with C: {C} and max_iter: {max_iter}")
    return model

def cross_validated_logistic_regression(X_train, y_train, Cs):
    print(f"Starting cross-validated logistic regression with Cs: {Cs}")
    logistic_regression_cv = LogisticRegressionCV(
        Cs=Cs,
        cv=StratifiedKFold(n_splits=4),
        solver='newton-cg',
        multi_class='multinomial',
        max_iter=1000,
        scoring='accuracy',
        n_jobs=-1
    )
    logistic_regression_cv.fit(X_train, y_train)
    print(f"Completed cross-validated logistic regression with Cs: {Cs}")
    return logistic_regression_cv

def calculate_accuracy(y_true, y_pred):
    print("Calculating accuracy")
    return accuracy_score(y_true, y_pred)

from sklearn.metrics import log_loss, make_scorer

def log_loss_scorer():
    print("Creating log loss scorer")
    return make_scorer(log_loss, greater_is_better=False, response_method='predict_proba')

def calculate_cross_val_log_loss(model, X, y, cv=4):
    print("Starting to calculate cross-validated log loss")
    scorer = log_loss_scorer()
    scores = cross_val_score(model, X, y, cv=cv, scoring=scorer, n_jobs=-1)
    print("Completed calculating cross-validated log loss")
    return scores.mean()


# Note: All function calls are commented out to align with the instruction for this task
# Example of a function call that would be commented out:
# result = unpickle('path/to/file')
