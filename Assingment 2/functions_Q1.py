"""
Created on Wed March 27 2024

@author:  Your names and student numbers
Rinze Hallema - 5280133
Klaudia Horvath - 6081487
Nadim Maraqten - 6117856
"""
# import packages

import numpy as np


# The logistic function
def logistic(x):
    return 1 / (1 + np.exp(-x))


# Simulate the labels
def logistic_simulation(features, beta, n):
    signal = np.dot(features, beta)
    p = logistic(signal)
    y = np.array([np.random.binomial(1, p[i]) for i in range(n)])
    return y


def calculate_gradient(X, W, beta, z):
    return (2 * np.transpose(X).dot(W)).dot(X.dot(beta) - z)


def calculate_hessian(X, W):
    return (2 * np.transpose(X).dot(W)).dot(X)


def calculate_beta_new(beta, X, W, z, lambda_par=0):
    hessian = calculate_hessian(X, W) + lambda_par * 2 * np.identity(
        np.shape(beta)[0])
    gradient = calculate_gradient(X, W, beta, z) + lambda_par * 2 * beta
    return (beta - np.linalg.inv(hessian).dot(gradient), gradient)


def calculate_mu(X, beta):
    # return 1/(1+np.exp(-beta.dot(X)))
    return 1 / (1 + np.exp(-X.dot(beta)))


def calculate_mu_matrix(X, beta):
    mu_matrix = np.zeros((np.shape(X)[0], 1))
    for i in range(np.shape(X)[0]):
        mu_matrix[i, :] = calculate_mu(X[i, :], beta)
    return mu_matrix.flatten()


def calculate_z(X, beta, Y, W, mu):
    return X.dot(beta) + np.linalg.inv(W).dot(Y - mu)


def calculate_w(mu):
    mu_diag = np.diag(mu)
    return mu_diag - mu_diag.dot(mu_diag)


def calculate_MLE_using_IRLS(beta_initial, W_initial, X, Y, num_of_iterations=20,
                             lambda_par=0):
    # Calculates MLE, stops when the maximum iterations are reached
    beta = beta_initial
    W = W_initial
    gradient = 1000  # Initialize gradient
    for i in range(num_of_iterations):
        mu = calculate_mu_matrix(X, beta)
        z = calculate_z(X, beta, Y, W, mu)
        W = calculate_w(mu)
        beta = calculate_beta_new(beta, X, W, z, lambda_par)[0]
        gradient = calculate_beta_new(beta, X, W, z, lambda_par)[1]
    return beta


# given an estimated parameter for the logistic model we can obtain forecasts
# for our test set with the following function

def logistic_forecast(features, beta):
    signal_hat = np.dot(features, beta)
    y_hat = np.sign(signal_hat)
    y_hat[y_hat < 0] = 0
    return y_hat


# computes the prediction error by comparing the predicted and the observed labels
# in the test set
def prediction_accuracy(y_predicted, y_observed):
    errors = np.abs(y_predicted - y_observed)
    total_errors = sum(errors)
    acc = 1 - total_errors / len(y_predicted)
    return acc


def logistic_regression_NR(features, target, num_steps, tolerance):
    beta = np.zeros(features.shape[1])
    return beta


def logistic_regression_NR_penalized(features, target, num_steps, tolerance):
    beta = np.zeros(features.shape[1])
    return beta
