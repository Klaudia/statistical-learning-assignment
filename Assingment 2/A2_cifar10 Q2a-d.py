# A2_cifar10 Q2a-d.py

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import LogLocator
import os
from functions_Q2 import load_data, reduce_dataset, scale_data, apply_pca, perform_logistic_regression, cross_validated_logistic_regression, plot_image_from_data, calculate_accuracy, calculate_cross_val_log_loss

"""
TASK 2a
"""

# Set the directory where the script is located as the working directory
script_dir = os.path.dirname(__file__)
data_dir = os.path.join(script_dir, "data/cifar-10-batches-py")

# Load the dataset
X_train_complete, y_train_complete, X_test_complete, y_test_complete, label_names = load_data(data_dir)

# Optional: plot a CIFAR-10 image to verify data loading
plot_image_from_data(X_train_complete, y_train_complete, 320, label_names)

"""
TASK 2b
"""

# Data reduction
reduction_factor = 0.9
X_train_reduced, y_train_reduced = reduce_dataset(X_train_complete, y_train_complete, reduction_factor)
X_test_reduced, y_test_reduced = reduce_dataset(X_test_complete, y_test_complete, reduction_factor)

# Data scaling
X_train_scaled, X_test_scaled = scale_data(X_train_reduced, X_test_reduced)

# PCA application
n_components = 200
X_train_pca, X_test_pca, pca = apply_pca(X_train_scaled, X_test_scaled, n_components)

# Perform logistic regression
C = 1.0
logistic_regression_model = perform_logistic_regression(X_train_pca, y_train_reduced, C)

# Predictions and accuracy calculation
y_pred = logistic_regression_model.predict(X_test_pca)
accuracy = calculate_accuracy(y_test_reduced, y_pred)
print(f"Accuracy of the logistic regression classifier on the test set: {accuracy:.2f}")

"""
TASK 2c
"""

# Cross-validated logistic regression
Cs = np.logspace(-9, 1, 20)
logistic_regression_cv_model = cross_validated_logistic_regression(X_train_pca, y_train_reduced, Cs)

# Plotting cross-validation results
mean_scores = np.array(logistic_regression_cv_model.scores_[1]).mean(axis=0)

# create figure
plt.figure()
plt.loglog(Cs, mean_scores, marker='o')
plt.xlabel('Regularization parameter C')
plt.ylabel('Cross-validated accuracy')
plt.title('Cross-Validated Accuracy vs Regularization Parameter')
ticks = np.logspace(-10, 2, 13)
plt.xticks(ticks, [f"$10^{{{int(np.log10(tick))}}}$" for tick in ticks], fontsize=8)
# Enabling grid and minor ticks
plt.grid(True, which="both", ls="-")  # Major grid lines
plt.minorticks_on()  # This enables the minor ticks
# Calculate minor tick positions manually for the logarithmic scale
minor_ticks = np.hstack([np.linspace(10**exp, 10**(exp+1), 10, endpoint=False)[1:] for exp in range(-10, 2)])
plt.gca().set_xticks(minor_ticks, minor=True)
# Customizing minor grid appearance
plt.grid(True, which="minor", axis='x', linestyle=':', linewidth=0.5)
plt.tight_layout()
plt.savefig(os.path.join(script_dir, 'cv_accuracy_vs_C.png'))
plt.show()

"""
TASK 2d
"""

# Calculate and plot log-loss for different values of C using cross-validation
mean_log_loss_scores = []
for C in Cs:
    model = perform_logistic_regression(X_train_pca, y_train_reduced, C=C)
    mean_log_loss = calculate_cross_val_log_loss(model, X_train_pca, y_train_reduced)
    mean_log_loss_scores.append(-mean_log_loss)  # Negate because lower log-loss is better


# create plot
fig, ax1 = plt.subplots()
# Use ax1 for accuracy, so it's on the left side
color = 'tab:blue'
ax1.set_xlabel('Regularization parameter C')
ax1.set_ylabel('Cross-validated accuracy', color=color)
ax1.loglog(Cs, mean_scores, marker='o', color=color)
ax1.tick_params(axis='y', labelcolor=color)
# Setting major x ticks for ax1
ticks = np.logspace(-10, 2, 13)
ax1.set_xticks(ticks)
ax1.set_xticklabels([f"$10^{{{int(np.log10(tick))}}}$" for tick in ticks], fontsize=8)
# Create a second y-axis for log-loss
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:red'
ax2.set_ylabel('Mean Cross-validated Log-Loss', color=color)
ax2.loglog(Cs, mean_log_loss_scores, marker='o', color=color)  # Assume mean_log_loss_scores is defined
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # Adjust layout to make room for the second y-label
fig.suptitle('CV Log-Loss and CV Accuracy vs Regularization Parameter', va='bottom')
# Enabling grid and minor ticks for ax1
ax1.grid(True, which="both", ls="-")  # Major grid lines for ax1
ax1.minorticks_on()  # Enable minor ticks for ax1
# Calculate and set minor tick positions manually for the logarithmic scale on ax1
minor_ticks = np.hstack([np.linspace(10**exp, 10**(exp+1), 10, endpoint=False)[1:] for exp in range(-10, 2)])
ax1.set_xticks(minor_ticks, minor=True)
# Customizing minor grid appearance for ax1
ax1.grid(True, which="minor", axis='x', linestyle=':', linewidth=0.5)
plt.tight_layout()
fig.savefig(os.path.join(script_dir, 'CV_log_loss_and_CV_accuracy_vs_C.png'))  # Save the figure as a PNG file before showing it
plt.show()

# chose best C
index_C_optimal = np.argmax(mean_scores)
C_optimal = Cs[index_C_optimal]
print(f"The optimal C value is: {C_optimal}, which corresponds to an accuracy of {mean_scores[index_C_optimal]} and log-loss of {mean_log_loss_scores[index_C_optimal]}")

