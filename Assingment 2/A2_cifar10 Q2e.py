import numpy as np 
import os
from functions_Q2 import load_data, plot_image_from_data, scale_data, apply_pca, perform_logistic_regression, calculate_accuracy
from sklearn.metrics import log_loss

# get file path
script_dir = os.path.dirname(__file__)
data_dir = os.path.join(script_dir, "data/cifar-10-batches-py")

# Load data using functions from functions_Q2.py
X_train_complete, y_train_complete, X_test_complete, y_test_complete, label_names = load_data(data_dir)

# Plot an image for verification
print("Now plotting test image. Close image to continue")
image_nr = 320
plot_image_from_data(X_train_complete, y_train_complete, image_nr, label_names)
print("Image plotted successfully")

# Scale data
X_train_scaled, X_test_scaled = scale_data(X_train_complete, X_test_complete)

# Apply PCA
n_components = 200
X_train_pca, X_test_pca, pca = apply_pca(X_train_scaled, X_test_scaled, n_components)

# Prepare data for the model
X_train = X_train_pca
y_train = y_train_complete
X_test = X_test_pca
y_test = y_test_complete

# Fit model with C_optimal
C_optimal = 0.0006
logistic_regression_model = perform_logistic_regression(X_train, y_train, C=C_optimal)

# Predict on training and test sets
y_train_pred = logistic_regression_model.predict(X_train)
y_test_pred = logistic_regression_model.predict(X_test)

# Calculate accuracy for training and test sets
train_accuracy = calculate_accuracy(y_train, y_train_pred)
test_accuracy = calculate_accuracy(y_test, y_test_pred)

# Calculate log loss for training and test sets
train_log_loss = log_loss(y_train, logistic_regression_model.predict_proba(X_train))
test_log_loss = log_loss(y_test, logistic_regression_model.predict_proba(X_test))

results_text = f"""
Training Accuracy: {train_accuracy:.2f}
Test Accuracy: {test_accuracy:.2f}
Training Log Loss: {train_log_loss:.2f}
Test Log Loss: {test_log_loss:.2f}
"""

# Print and save the results
print(results_text)
results_file_path = os.path.join(script_dir, 'model_evaluation_results.txt')
with open(results_file_path, 'w') as f:
    f.write(results_text)
print(f"Results saved to {results_file_path}")
