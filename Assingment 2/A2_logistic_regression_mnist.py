#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed March 27 2024

@author:  Your names and student numbers
Rinze Hallema - 5280133
Klaudia Horvath - 6081487
Nadim Maraqten - 6117856
"""

# import packages

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from functions_Q1 import logistic_forecast, prediction_accuracy, \
    calculate_MLE_using_IRLS

# read the csv file
df = pd.read_csv('mnist.csv')

# make dataframe into numpy array
df.to_numpy()
y_labels_data = df['label'].to_numpy()
df_xdata = df.drop(columns='label')
x_features_data = df_xdata.to_numpy()
x_features_data.shape

# we will only use the zeros and ones in this empirical study
y_labels_01 = y_labels_data[np.where(y_labels_data <= 1)[0]]
x_features_01 = x_features_data[np.where(y_labels_data <= 1)[0]]

# create training set
n_train = 100
y_train = y_labels_01[0:n_train]
x_train = x_features_01[0:n_train]
print("The rank of the training set is: {}".format(np.linalg.matrix_rank(x_train)))

beta_initial = np.zeros((784,))
W_initial = np.eye(n_train)
# Exercise 1 c
# using no regularization leads to "singular matrix" error
# beta_calc = calculate_MLE_using_IRLS(beta_initial, W_initial, x_train, y_train,
#                                     num_of_iterations=20,
#                                     lambda_par=0)

# Exercise 1 d
# Now we use labda parameter 1 to avoid singularity
beta_calc = calculate_MLE_using_IRLS(beta_initial, W_initial, x_train, y_train,
                                     num_of_iterations=20,
                                     lambda_par=1)

# create test set
n_total = y_labels_01.size
y_test = y_labels_01[n_train:n_total]
x_test = x_features_01[n_train:n_total]

## Here we plot some handwritten digits
plt.figure(figsize=(25, 5))
for index, (image, label) in enumerate(zip(x_train[5:10], y_train[5:10])):
    plt.subplot(1, 5, index + 1)
    plt.imshow(np.reshape(image, (28, 28)), cmap=plt.cm.gray)
    plt.title('Label: %i\n' % label, fontsize=20)

# Regularisation parameter
lambda_0 = 1

y_predicted = logistic_forecast(x_test, beta_calc)
acc = prediction_accuracy(y_predicted, y_test)
print("The prediction accuracy is: {}".format(acc))
print("The number of non-zero elements in beta is: {}".format(
    np.size(np.nonzero(beta_calc))))

f1 = plt.figure(figsize=(12, 8))
plt.hist(beta_calc, bins=6)
f1.savefig('Beta_calculated.png')
